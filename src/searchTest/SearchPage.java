package searchTest;

import pageObjects.Product;
import pageObjects.EBayPage;
import pageObjects.ShopeePage;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchPage {
	
	WebDriver driver; 
	EBayPage eBay;
	ShopeePage shopee;
	ArrayList<Product> pd =  new ArrayList<Product>();
	ArrayList<Product> pd2 =  new ArrayList<Product>();
	
	String searchInput = "Iphone 11";	
	String url1 = "https://www.ebay.com/";
	String url2 = "https://shopee.com.my/";
	String priceRange = "2000";

	@BeforeMethod
    public void setup(){
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
    }
	
	@Test
	public void ebaySearchTest() {

		eBay = new EBayPage(driver);
		driver.get(url1);
		
		eBay.searchItem(searchInput);
		eBay.customizePage();
		eBay.clickSeeAllLink();
		eBay.applyStorageFilter();
		eBay.clickApplyFilter();
		eBay.clickSeeAllLink();
		eBay.applyModelFilter();
		eBay.clickApplyFilter();
		eBay.clickSeeAllLink();
		eBay.applyColorFilter();
		eBay.clickApplyFilter();
		eBay.clickSeeAllLink();
		eBay.applyOSFilter();
		eBay.clickApplyFilter();
		eBay.getSearchDetails(pd);
	}
	
	@Test
	public void shopeeSearchTest() {

		shopee = new ShopeePage(driver);
		driver.get(url2);
		
		shopee.escapeLangPopUp();
		shopee.searchItem(searchInput);
		shopee.filterByCategory();
		shopee.filterByBrand();
		shopee.definePriceRange(priceRange);
		shopee.getSearchDetails(pd2);
	}
	
	
	@AfterTest
	public void printSortedProduct() {
		pd.addAll(pd2);
		Collections.sort(pd);
		System.out.println("-----------------------------------------Sorted List-----------------------------------------");

		DecimalFormat df = new DecimalFormat("0.00");
		for(Product str:pd) {
			if (str.getProductLink().contains(url1)) 
				System.out.println("Website : "+url1);			
			else 
				System.out.println("Website : "+url2);	
			System.out.println("Product Name : "+str.getProductName());
			System.out.println("Product Link : "+str.getProductLink());
			System.out.println("Product Price : RM"+df.format(str.getPriceD()));
			System.out.println("");
		}
	}
	  
	@AfterMethod
	public void closeDriver() {
		driver.quit();
	}
}
