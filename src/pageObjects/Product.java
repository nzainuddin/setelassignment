package pageObjects;

import java.util.ArrayList;

public class Product implements Comparable<Product>{
	ArrayList<Product> pd;
	ArrayList<Product> pd2;

	public String productLink;
	public String productName;
	public double priceD;
	
	public Product(String productName, String productLink, double priceD) {
		this.productName = productName;
		this.productLink = productLink;
		this.priceD = priceD;
	}
	
	public String getProductLink() {
		return productLink;
	}
	
	public void setProductLink(String productLink) {
		this.productLink = productLink;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public double getPriceD() {
	    return priceD;
	}
	
	public void setPriceD(double priceD) {
		this.priceD = priceD;
	}
	
	@Override 
	public int  compareTo(Product o) {
	    return new Double(this.priceD).compareTo(new Double(o.priceD));
	}
}
