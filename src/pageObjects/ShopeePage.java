package pageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.Helper;

public class ShopeePage {

	WebDriver driver;
	WebDriverWait wait;
	Actions actions;
	JavascriptExecutor js;
	Helper helpers;
	
	// Input
	@FindBy(css = ".shopee-searchbar-input input")
	WebElement searchInput;
	
	@FindBy(css = ".shopee-price-range-filter__inputs input:nth-child(1)")
	WebElement priceRangeInput;
	
	// Button
	@FindBy(css = ".language-selection__list-item:nth-child(1) button")
	WebElement englishLangBtn;
	
	@FindBy(css = ".shopee-searchbar .btn")
	WebElement searchBtn;
	
	@FindBy(css = ".shopee-popup__close-btn")
	WebElement popUpXBtn;
	
	@FindBy(css = "[class*='facet-filter'] .shopee-filter-group__toggle-btn")
	WebElement byCategoryMoreBtn;
	
	@FindBy(css = ".shopee-icon-button--right")
	WebElement nextPageBtn;
	

	// Checkbox
	@FindBy(css = ".shopee-facet-filter  div:nth-child(1) > div.shopee-checkbox-filter:nth-child(4) > div > label")
	WebElement mobileCategoryCbx;
	
	@FindBy(css = ".shopee-brands-filter  div.shopee-checkbox-filter:nth-child(1) > div > label")
	WebElement appleBrandCbx;
	
	@FindBy(css = "[class*='price-range-filter'] button")
	WebElement applyPriceRangeBtn;
	
	// Label
	@FindBy(css = "[data-sqe='name'] > div:nth-child(1)")
	List<WebElement>  productTitleLbl;
	
	@FindBy(css = "[data-sqe='name'] + div span:nth-child(2)")
	List<WebElement>  productPriceLbl;
	
	@FindBy(css = ".shopee-search-item-result__item [data-sqe='link']")
	List<WebElement>  productLink;
	
	@FindBy(css = ".shopee-search-item-result__item")
	List<WebElement>  productItem;
	
	public ShopeePage(WebDriver driver){ 
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 30);
        actions = new Actions(driver);
        js  = (JavascriptExecutor) driver;
        helpers = new Helper(driver);
	} 

	public boolean checkVisibility(WebElement element) {
		try {
			element.isDisplayed();
			System.out.println("exist");
			return true;
		} catch (NoSuchElementException e) {
			System.out.println("Not exist");
			return false;
		}
	}
	
	public void escapeLangPopUp() {
		if(checkVisibility(this.englishLangBtn) == true) {
			helpers.clickOnceClickable(this.englishLangBtn);
			actions.sendKeys(Keys.ESCAPE).build().perform();
		} else {
			actions.sendKeys(Keys.ESCAPE).build().perform();
		}
	}
		
	public void searchItem(String input) {
		this.searchInput.sendKeys(input);
		this.searchBtn.click();
	}
	
	public void filterByCategory() {
		helpers.clickOnceClickable(this.byCategoryMoreBtn);
		helpers.clickOnceVisible(this.mobileCategoryCbx);
	}
	
	public void filterByBrand() {
		this.appleBrandCbx.click();
	}
	
	public void definePriceRange(String price) {
		this.priceRangeInput.sendKeys(price);
		helpers.clickOnceClickable(this.applyPriceRangeBtn);
	}
	
	public void getSearchDetails(ArrayList<Product> pd2) {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		int itemPerPage = 50;
		wait.until(ExpectedConditions.visibilityOfAllElements(this.productItem.get(itemPerPage - 1)));
		for (int i = 0; i < itemPerPage; i++) {
			// Need to scroll, as item might not fully loaded
			js.executeScript("arguments[0].scrollIntoView();", this.productItem.get(i));
			String productName = this.productTitleLbl.get(i).getText();
			String productLink = this.productLink.get(i).getAttribute("href");
			String price = this.productPriceLbl.get(i).getText();
			Double priceD = Double.parseDouble(price.replace(",", ""));

			pd2.add(new Product(productName, productLink, priceD));
		}

	}
	
}
