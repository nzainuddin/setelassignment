package pageObjects;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import util.Helper;

public class EBayPage {
	
	WebDriver driver;
	WebDriverWait wait;
	Actions actions;
	Helper helpers;

	//	Input
	@FindBy(css = "label + [class*='autocomplete-input']")
	WebElement searchInput;
	
	
	//	Label
	@FindBy(css = ".srp-results .s-item__title")
	List<WebElement> productTitleLbl;
	
	@FindBy(css = ".srp-results div:nth-child(1) > .s-item__price")
	List<WebElement> productPriceLbl1;
	
	@FindBy(css = ".srp-results div:nth-child(5) > .s-item__price")
	List<WebElement> productPriceLbl2;
	
	@FindBy(css = ".srp-results .s-item__bids")
	WebElement productBidsLbl;
	
	
	//	Button
	@FindBy(css = "input[value='Search']")
	WebElement searchBtn;
	
	@FindBy(css = ".srp-results .s-item__link")
	List<WebElement> productLink;
	
	@FindBy(className = "pagination__next")
	WebElement nextPageBtn;
	
	@FindBy(css = ".pagination__next[aria-disabled = 'true']")
	WebElement nextPageDisabledBtn;
	
	@FindBy(css = ".s-customize__options label:nth-child(4)")
	WebElement perPage200Opt;
	
	@FindBy(css = "[class*='customize'] [type='submit']")
	WebElement applyChangesBtn;
	
	@FindBy(css = "[aria-label='Apply']")
	WebElement applyFilterBtn;
	
	@FindBy(css = "[class*='main__list']:nth-child(1) button[aria-label*='see all']")
	WebElement seeAllLink;
	
	@FindBy(css = "[data-aspecttitle*='Storage'] span")
	WebElement storageOptMenu;
	
	@FindBy(css = "[data-aspecttitle='aspect-Model'] span")
	WebElement modelOptMenu;
	
	@FindBy(css = "[data-aspecttitle*='Brand'] span")
	WebElement brandOptMenu;
	
	@FindBy(css = "[data-aspecttitle*='aspect-Color'] span")
	WebElement colorOptMenu;
	
	@FindBy(css = "[data-aspecttitle*='aspect-Operating'] span")
	WebElement OSOptMenu;
	
	
	//	Checkbox
	@FindBy(css = "[for*='Capacity_64%20GB']")
	WebElement storage64GBCbx;
	
	@FindBy(css = "[for*='Capacity_128%20GB']")
	WebElement storage128GBCbx;
	
	@FindBy(css = "[for*='Capacity_256%20GB']")
	WebElement storage256GBCbx;
	
	@FindBy(css = "[for*='Capacity_512%20GB']")
	WebElement storage512GBCbx;
	
	@FindBy(css = "[for*='iPhone%2011-']")
	WebElement modelIphone11Cbx;
	
	@FindBy(css = "[for*='iPhone%2011%20Pro-']")
	WebElement modelIphone11ProCbx;
	
	@FindBy(css = "[for*='iPhone%2011%20Pro%20Max']")
	WebElement modelIphone11ProMaxCbx;
	
	@FindBy(css = "[id*='Apple'] [for*='Brand_Apple']")
	WebElement brandAppleCbx;
	
	@FindBy(css = "[id*='Black'] [for*='Color_Black']")
	WebElement colorBlackCbx;
	
	@FindBy(css = "[id*='Gold'] [for*='Color_Gold']")
	WebElement colorGoldCbx;
	
	@FindBy(css = "[id*='Gray'] [for*='Color_Gray']")
	WebElement colorGrayCbx;
	
	@FindBy(css = "[id*='Green'] [for*='Color_Green']")
	WebElement colorGreenCbx;
	
	@FindBy(css = "[id*='Purple'] [for*='Color_Purple']")
	WebElement colorPurpleCbx;
	
	@FindBy(css = "[id*='Red'] [for*='Color_Red']")
	WebElement colorRedCbx;
	
	@FindBy(css = "[id*='Silver'] [for*='Color_Silver']")
	WebElement colorSilverCbx;
	
	@FindBy(css = "[id*='White'] [for*='Color_White']")
	WebElement colorWhiteCbx;
	
	@FindBy(css = "[id*='Yellow'] [for*='Color_Yellow']")
	WebElement colorYellowCbx;
	
	@FindBy(css = "[id*='iOS'] [for*='iOS']")
	WebElement OSiOSCbx;
	
	
	//	Dropdown
	@FindBy(css = ".srp-controls__sort button")
	WebElement sortDD;
	
	@FindBy(css = "span[class*='control']:nth-child(1) a[class*='menu-button__item']:nth-child(1)")
	WebElement bestMatchSortOpt;
	
	@FindBy(css = "span[class*='control']:nth-child(1) a[class*='menu-button__item']:nth-child(2)")
	WebElement endSoonSortOpt;
	
	@FindBy(css = "span[class*='control']:nth-child(1) a[class*='menu-button__item']:nth-child(3)")
	WebElement newestSortOpt;
	
	@FindBy(css = "span[class*='control']:nth-child(1) a[class*='menu-button__item']:nth-child(4)")
	WebElement lowToHighSortOpt;
	
	@FindBy(css = "span[class*='control']:nth-child(1) a[class*='menu-button__item']:nth-child(5)")
	WebElement highToLowSortOpt;
	
	@FindBy(css = "span[class*='control']:nth-child(1) a[class*='menu-button__item']:nth-child(6)")
	WebElement nearestSortOpt;
	
	@FindBy(css = ".btn__icon")
	WebElement settingsDD;
	
	@FindBy(css = "[class*='options__customize']")
	WebElement settingsCustomizeItem;
	
	
	public EBayPage(WebDriver driver){ 
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 15);
        actions = new Actions(driver);
        helpers = new Helper(driver);
	} 

	public void searchItem(String input) {
		this.searchInput.sendKeys(input);
		this.searchBtn.click();
	}
	
	public void sortBy(String input) {
		this.sortDD.click();
		if (input == "Best Match") {
			this.bestMatchSortOpt.click();
		} else if (input == "Ending Soon") {
			this.endSoonSortOpt.click();
		} else if (input == "New") {
			this.newestSortOpt.click();
		} else if (input == "Low First") {
			this.lowToHighSortOpt.click();
		} else if (input == "High First") {
			this.highToLowSortOpt.click();
		} else if (input == "Nearest") {
			this.nearestSortOpt.click();
		}
	}
	
	public void customizePage() {
		helpers.clickOnceClickable(this.settingsDD);
		this.settingsCustomizeItem.click();
		helpers.clickOnceVisible(this.perPage200Opt);
		this.applyChangesBtn.click();
	}
	
	public void clickSeeAllLink() {
		wait.until(ExpectedConditions.elementToBeClickable(seeAllLink));
		actions.moveToElement(seeAllLink).perform();
		this.seeAllLink.click();
	}
	
	public void applyStorageFilter() {
		helpers.clickOnceClickable(this.storageOptMenu);
		helpers.clickOnceVisible(this.storage64GBCbx);
		this.storage128GBCbx.click();
		this.storage256GBCbx.click();
		this.storage512GBCbx.click();
	}
	
	public void applyModelFilter() {
		helpers.clickOnceClickable(this.modelOptMenu);
		helpers.clickOnceVisible(this.modelIphone11Cbx);
		this.modelIphone11ProCbx.click();
		this.modelIphone11ProMaxCbx.click();
	}
	
	public void applyBrandFilter() {
		helpers.clickOnceClickable(this.brandOptMenu);
		helpers.clickOnceClickable(this.brandAppleCbx);
	}
	
	public void applyColorFilter() {
		helpers.clickOnceClickable(this.colorOptMenu);
		helpers.clickOnceVisible(this.colorBlackCbx);
		this.colorGoldCbx.click();
		this.colorGrayCbx.click();
		this.colorGreenCbx.click();
		this.colorPurpleCbx.click();
		this.colorRedCbx.click();
		this.colorSilverCbx.click();
		this.colorWhiteCbx.click();
		this.colorYellowCbx.click();
	}
	
	public void applyOSFilter() {
		helpers.clickOnceClickable(this.OSOptMenu);
		helpers.clickOnceVisible(this.OSiOSCbx);
	}
	
	public void clickApplyFilter() {
		helpers.clickOnceClickable(this.applyFilterBtn);
	}
	
	public void getSearchDetails(ArrayList<Product> pd) {
		int itemPerPage = 50;
		double rateUSD = 4.15;
		wait.until(ExpectedConditions.visibilityOfAllElements(this.productLink.get(itemPerPage - 1)));
		wait.until(ExpectedConditions.visibilityOfAllElements(this.productTitleLbl.get(itemPerPage - 1)));
		for (int i = 0; i < itemPerPage; i++) {
			String productName = this.productTitleLbl.get(i).getText();
			String productLink = this.productLink.get(i).getAttribute("href");
			String price = this.productPriceLbl1.get(i).getText();
			Double priceD = Double.parseDouble(price.split(" ")[0].replace("$", "").replace(",", ""));
			priceD *= rateUSD;

			pd.add(new Product(productName, productLink, priceD));
		}
	}
}
