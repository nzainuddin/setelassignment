package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Helper {
	WebDriver driver;
	WebDriverWait wait;
	
	public Helper(WebDriver driver){ 
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
	} 
	
	public void clickOnceClickable(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}
	
	public void clickOnceVisible(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
		element.click();
	}
}
