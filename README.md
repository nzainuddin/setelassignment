### SetelAssignment

## Setel automation assignment for web (eBay & Shopee page)

1. Clone the project into your local folder 
```
git clone <repository-clone-link> <folder-name>
```
2. Navigate to the cloned repository directory on local machine
3. To run the test, simply right click `classes-test-testng.xml` file under `src` folder to execute the test in parallel (**Run As > TestNG Suite**)
4. Else, run the test by right click the `SearchPage` file under `src/searchTest` package to execute the test (**Run As > TestNG Test**)




---



### Test Result Example:

```
...

Website : https://www.ebay.com/
Product Name : Apple iPhone 11 Pro - 256GB - Midnight Green (Unlocked) A2215 (GSM) Brand New!
Product Link : https://www.ebay.com/itm/Apple-iPhone-11-Pro-256GB-Midnight- ...
Product Price : RM4897.00

Website : https://shopee.com.my/
Product Name : Iphone 11 Pro Max (64GB) (256GB) (512GB) (Apple Malaysia Warranty) Free Extra Original Cable
Product Link : https://shopee.com.my/Iphone-11-Pro-Max-(64GB)-(256GB) ...
Product Price : RM5299.00

Website : https://shopee.com.my/
Product Name : Apple iPhone 11 Pro Max (64GB/256GB/512GB)
Product Link : https://shopee.com.my/Apple-iPhone-11-Pro-Max-(64GB-256GB-512GB)-i.583 ...
Product Price : RM5499.00

Website : https://www.ebay.com/
Product Name : NEW LISTING2020 APPLE iPhone 11 WITH FORTNITE APP Installed BOX 64GB UNLOCKED Game WHITE X
Product Link : https://www.ebay.com/itm/2020-APPLE-iPhone-11-WITH-FORTNITE-APP-Install ...
Product Price : RM11537.00

...

PASSED: ebaySearchTest
PASSED: shopeeSearchTest

===============================================
    Default test
    Tests run: 2, Failures: 0, Skips: 0
===============================================


```
